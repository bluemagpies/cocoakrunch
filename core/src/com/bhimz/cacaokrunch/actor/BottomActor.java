package com.bhimz.cacaokrunch.actor;/**
 * Created by Lenovo on 10/22/2015.
 */

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.bhimz.cacaokrunch.util.GameUtil;

public class BottomActor extends Actor {
    private Texture texture;
    public BottomActor(float width, float height) {
        super();
        Pixmap p = new Pixmap(Math.round(width), Math.round(height), Pixmap.Format.RGBA8888);
        p.setColor(GameUtil.parseColor("0xf1c40f"));
        p.fillRectangle(0, 0, Math.round(width), Math.round(height));
        texture = new Texture(p);
        p.dispose();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(texture, getX(), getY());
    }

    public void act(float delta) {
        super.act(delta);
    }

}