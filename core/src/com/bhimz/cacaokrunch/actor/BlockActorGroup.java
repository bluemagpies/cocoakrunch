package com.bhimz.cacaokrunch.actor;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.bhimz.cacaokrunch.GameConstant;
import com.bhimz.cacaokrunch.util.ActorAccessor;
import com.bhimz.cacaokrunch.util.GameUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.equations.Bounce;

public class BlockActorGroup extends Group implements InputProcessor {
    private Image cover;

    private Map<Integer, BlockActor> actorMap = new HashMap<Integer, BlockActor>();
    private List<BlockActor> blockPool = new ArrayList<BlockActor>();
    private BlockActor[][] board = new BlockActor[GameConstant.BlockConfig.colSize][GameConstant.BlockConfig.rowSize * 2];
    Random random = new Random(System.currentTimeMillis());

    private int startingRow = GameConstant.BlockConfig.rowSize;

    public BlockActorGroup(float x, float y) {
        super();
        setBounds(x, y, GameConstant.BlockConfig.boardWidth, GameConstant.BlockConfig.boardHeight);
        setPosition(x, y);
        int totalBlocks = GameConstant.BlockConfig.rowSize * GameConstant.BlockConfig.colSize;

        BlockActor blockActor;
        for (int i = 0; i < totalBlocks; i++) {
            blockActor = new BlockActor(i);
            actorMap.put(i, blockActor);
            blockPool.add(blockActor);
        }

        for (int i = startingRow; i < startingRow + 4; i++) {
            for (int j = 0; j < GameConstant.BlockConfig.colSize; j++) {
                board[j][i] = poolBlock();
                board[j][i].blockType = random.nextInt(4);
                board[j][i].setPosition(j * 60, i * 60);
                addActor(board[j][i]);
            }
        }

        Pixmap p = new Pixmap(Math.round(GameConstant.SCREEN_WIDTH), Math.round(GameConstant.BlockConfig.boardHeight), Pixmap.Format.RGBA8888);
        p.setColor(GameUtil.parseColor("0x00000000"));
        p.fillRectangle(0, 0, Math.round(GameConstant.SCREEN_WIDTH), Math.round(GameConstant.BlockConfig.boardHeight));
        Texture t = new Texture(p);
        cover = new Image(t);
        cover.setBounds(0,0, GameConstant.SCREEN_WIDTH, GameConstant.BlockConfig.boardHeight);
        cover.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int row = (int) Math.floor(y / GameConstant.BlockConfig.blockSize);
                int col = (int) Math.floor(x / GameConstant.BlockConfig.blockSize);

                if (row >= 0 && row < GameConstant.BlockConfig.rowSize && col >= 0 && col < GameConstant.BlockConfig.colSize) {
                    BlockActor blockActor = board[col][row];
                    System.out.println("clicked[" +row+"]["+col+"]");
                    if (blockActor != null) {
                    }
                }
            }
        });
        p.dispose();
        addActor(cover);
    }

    public void popNextLine() {
        BlockActor[] actors;
        for (int i = 0; i < GameConstant.BlockConfig.colSize; i++) {
            actors = Arrays.copyOfRange(board[i], 1, board[i].length);
            board[i] = Arrays.copyOf(actors, GameConstant.BlockConfig.rowSize * 2);
        }
    }

    private BlockActor poolBlock() {
        return blockPool.remove(0);
    }

    private void returnToPool(BlockActor blockActor) {
        blockPool.add(blockActor);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    private static class BlockActor extends Actor {
        private String[] spriteNames = {"cocoa_a", "cocoa_b", "cocoa_c", "cocoa_d", "cocoa_e"};
        TextureRegion[] blocks = new TextureRegion[spriteNames.length];
        public int blockType = 0;
        private Integer id;
        public BlockActor(final Integer id) {
            super();
            this.id = id;
            for (int i = 0; i < spriteNames.length; i++) {
                blocks[i] = GameUtil.textureAtlas.findRegion(spriteNames[i]);
            }
            setBounds(getX(), getY(), blocks[0].getRegionWidth(), blocks[0].getRegionHeight());
            setOrigin(30, 30);
            /*setScale(0.5f, 0.5f);
            Tween.to(this, ActorAccessor.SCALE_XY, 1.0f)
                    .target(1.0f, 1.0f).ease(Bounce.OUT).delay(0.5f)
                    .start(GameUtil.getTweenManager());*/
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            super.draw(batch, parentAlpha);
           batch.draw(blocks[blockType], getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        }
    }
}