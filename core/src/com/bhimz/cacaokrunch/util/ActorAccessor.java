package com.bhimz.cacaokrunch.util;

import com.badlogic.gdx.scenes.scene2d.Actor;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * Created by Lenovo on 10/24/2015.
 */
public class ActorAccessor implements TweenAccessor<Actor> {
    public final static int POSITION_X = 0;
    public final static int POSITION_Y = 1;
    public final static int POSITION_XY = 2;
    public final static int SCALE_X = 3;
    public final static int SCALE_Y = 4;
    public final static int SCALE_XY = 5;

    @Override
    public int getValues(Actor actor, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case POSITION_X:
                returnValues[0] = actor.getX();
                return 1;
            case POSITION_Y:
                returnValues[0] = actor.getY();
                return 1;
            case POSITION_XY:
                returnValues[0] = actor.getX();
                returnValues[1] = actor.getY();
                return 2;
            case SCALE_X:
                returnValues[0] = actor.getScaleX();
                return 1;
            case SCALE_Y:
                returnValues[0] = actor.getScaleY();
                return 1;
            case SCALE_XY:
                returnValues[0] = actor.getScaleX();
                returnValues[1] = actor.getScaleY();
                return 2;
        }
        return -1;
    }

    @Override
    public void setValues(Actor actor, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case POSITION_X:
                actor.setX(returnValues[0]);
                break;
            case POSITION_Y:
                actor.setY(returnValues[0]);
                break;
            case POSITION_XY:
                actor.setPosition(returnValues[0], returnValues[1]);
                break;
            case SCALE_X:
                actor.setScaleX(returnValues[0]);
                break;
            case SCALE_Y:
                actor.setScaleY(returnValues[0]);
                break;
            case SCALE_XY:
                actor.setScale(returnValues[0], returnValues[1]);
                break;
        }
    }
}
