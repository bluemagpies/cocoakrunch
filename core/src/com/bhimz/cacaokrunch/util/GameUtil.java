package com.bhimz.cacaokrunch.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.bhimz.cacaokrunch.GameConstant;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

/**
 * Created by BhimZ on 10/22/2015.
 */
public class GameUtil {

    public static TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.internal("cocoakrunch.pack"), false);
    private static OrthographicCamera camera;
    private static TweenManager tweenManager;

    public static void initTweenEngine() {
        Tween.registerAccessor(Actor.class, new ActorAccessor());
        tweenManager = new TweenManager();
    }

    public static TweenManager getTweenManager() {
        return tweenManager;
    }

    public static Camera getCamera() {
        if (camera == null) {
            camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            camera.setToOrtho(true, GameConstant.SCREEN_WIDTH, GameConstant.SCREEN_HEIGHT);
        }
        return camera;
    }

    public static Color parseColor(String s) {
        if(s.startsWith("0x"))
            s = s.substring(2);

        if(s.length() < 8) { // AARRGGBB
            if (s.length() < 6)
                throw new IllegalArgumentException("String must have the form RRGGBB/AARRGGBB");
            else {
                s = "FF" + s;
            }
        }

        return colorFromHex(Long.parseLong(s, 16));
    }

    // Expects a hex value as integer and returns the appropriate Color object.
    // @param hex
    //            Must be of the form 0xAARRGGBB
    // @return the generated Color object
    private static Color colorFromHex(long hex)
    {
        float a = (hex & 0xFF000000L) >> 24;
        float r = (hex & 0xFF0000L) >> 16;
        float g = (hex & 0xFF00L) >> 8;
        float b = (hex & 0xFFL);

        return new Color(r/255f, g/255f, b/255f, a/255f);
    }
}
