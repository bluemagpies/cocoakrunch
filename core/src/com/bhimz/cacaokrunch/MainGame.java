package com.bhimz.cacaokrunch;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.bhimz.cacaokrunch.screen.GameScreen;
import com.bhimz.cacaokrunch.util.GameUtil;

public class MainGame extends Game {
	private Screen screen;
	
	@Override
	public void create () {
		GameUtil.initTweenEngine();
		screen = new GameScreen(this);
        setScreen(screen);
	}
}
