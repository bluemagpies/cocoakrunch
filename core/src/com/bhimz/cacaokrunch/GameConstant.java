package com.bhimz.cacaokrunch;

/**
 * Created by Lenovo on 10/22/2015.
 */
public class GameConstant {
    public static final float SCREEN_WIDTH = 480;
    public static final float SCREEN_HEIGHT = 800;

    public static class BlockConfig {
        public static final int rowSize = 10;
        public static final int colSize = 8;
        public static final float blockSize = 60;
        public static final float boardWidth = colSize * blockSize;
        public static final float boardHeight = rowSize * blockSize;
    }
}
