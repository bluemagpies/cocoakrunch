package com.bhimz.cacaokrunch.screen;/**
 * Created by Lenovo on 10/22/2015.
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.bhimz.cacaokrunch.GameConstant;
import com.bhimz.cacaokrunch.MainGame;
import com.bhimz.cacaokrunch.actor.BlockActorGroup;
import com.bhimz.cacaokrunch.actor.BottomActor;
import com.bhimz.cacaokrunch.util.GameUtil;

/**
 * Created by Lenovo on 10/22/2015.
 */
public class GameScreen implements Screen {
    protected Stage stage;
    protected MainGame mainGame;

    public GameScreen(MainGame mainGame) {
        this.mainGame = mainGame;
        stage = new Stage(new ScalingViewport(Scaling.fill, GameConstant.SCREEN_WIDTH, GameConstant.SCREEN_HEIGHT, GameUtil.getCamera()));
        Gdx.input.setInputProcessor(stage);

        //setup screen & actors here

        float posY = 70;
        BlockActorGroup group = new BlockActorGroup(0, posY);
        stage.addActor(group);
        Group coverGroup = new Group();
        float boardH = GameConstant.BlockConfig.boardHeight;
        float boardW = GameConstant.BlockConfig.boardWidth;
        float blockSize = GameConstant.BlockConfig.blockSize;
        coverGroup.setBounds(0, 0, boardW, boardH + blockSize);
        coverGroup.setPosition(0, posY + boardH);
        Image image;
        for (int i = 0; i < GameConstant.BlockConfig.colSize; i++) {
            image = new Image(GameUtil.textureAtlas.findRegion("cocoa_cover"));
            image.setPosition(i * blockSize, 0);
            //image.setBounds(i * GameConstant.BlockConfig.blockSize, GameConstant.BlockConfig.boardHeight - GameConstant.BlockConfig.blockSize, GameConstant.BlockConfig.blockSize, GameConstant.BlockConfig.blockSize);
            coverGroup.addActor(image);
        }
        stage.addActor(coverGroup);

        BottomActor bottomActor = new BottomActor(GameConstant.SCREEN_WIDTH, 70);
        bottomActor.setBounds(0, posY + boardH + blockSize, GameConstant.SCREEN_WIDTH, 70);
        stage.addActor(bottomActor);

        Gdx.input.setInputProcessor(stage);
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(group);
        Gdx.input.setInputProcessor(multiplexer);
    }



    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        GameUtil.getTweenManager().update(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}